import React from 'react';
import Header from '../header';
import Footer from '../footer';

export default function Layout({ children, error, renderError }) {
  return (
    <>
      <Header />
      <main className="main">{error ? renderError : children}</main>
      <Footer />
    </>
  );
}
