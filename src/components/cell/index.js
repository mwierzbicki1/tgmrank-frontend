import { navigate } from '@reach/router';
import React from 'react';
import { Trans } from '@lingui/macro';
import InView from 'react-intersection-observer';

import Link from '../link';
import Wrapper from '../cell-wrapper';
import TgmRank, { ProofType } from '../../tgmrank-api';

import './styles.css';
import Flag from '../flag';
import Tooltip from '@reach/tooltip';
import LongDate from '../date';
import TimeAgo from '../time-ago';
import { StatsRow } from '../leaderboard/stats';

import { Image as ImageIcon, Video as VideoIcon } from 'react-feather';

export function Number({ title, stat, children }) {
  return (
    <div className="number">
      {stat != null && (
        <>
          <div>{stat}</div>
          <div>{title ?? children}</div>
        </>
      )}
    </div>
  );
}

function LeaderboardRank({ playerName, rank, createdAt }) {
  const millisecondsInAWeek = 60 * 60 * 24 * 7 * 1000;
  const recencyCutoff = 2 * millisecondsInAWeek;
  let isRecent = createdAt
    ? Date.now() - Date.parse(createdAt) < recencyCutoff
    : null;

  let rankDisplay = rank ?? '—';

  return playerName ? (
    <Number stat={rankDisplay}>
      {isRecent ? (
        <Tooltip
          label={
            <>
              <LongDate>{createdAt}</LongDate> (<TimeAgo>{createdAt}</TimeAgo>)
            </>
          }
        >
          <span>
            <Trans>New</Trans>
          </span>
        </Tooltip>
      ) : null}
    </Number>
  ) : (
    <Number stat={rankDisplay}>
      <Trans>Rank</Trans>
    </Number>
  );
}

export function PlayerCell({ player }) {
  return (
    <InView triggerOnce={true}>
      {({ inView, ref, entry }) => (
        <div ref={ref}>
          <span className="leaderboard__item--player">
            {inView && (
              <img
                className="avatar__thumbnail"
                width="30"
                height="30"
                src={TgmRank.getAvatarLink(player.avatar, true)}
              />
            )}
            <span>
              {player.playerId != null ? (
                <Link to={`/player/${player.playerName}`}>
                  {player.playerName.trim()}
                </Link>
              ) : (
                <span>{player.playerName.trim()}</span>
              )}
              {inView && <Flag code={player.location} />}
            </span>
          </span>
        </div>
      )}
    </InView>
  );
}

export default function Cell({ stats, score, disableClick }) {
  let player = score?.player;
  let playerName = player?.playerName;

  return (
    <Wrapper
      onClick={
        !disableClick
          ? () => score.scoreId && navigate(`/score/${score.scoreId}`)
          : null
      }
      className="leaderboard__item"
    >
      <LeaderboardRank
        playerName={playerName}
        rank={score?.rank}
        createdAt={score?.createdAt}
      />
      {playerName ? <PlayerCell player={player} /> : <span />}
      {score?.proof?.some(p => p?.type === ProofType.Video) ? (
        <VideoIcon size={18} />
      ) : score?.proof?.some(p => p?.type === ProofType.Image) ? (
        <ImageIcon size={18} />
      ) : (
        <div />
      )}
      <StatsRow stats={stats} item={score} />
    </Wrapper>
  );
}
