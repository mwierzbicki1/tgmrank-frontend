import React from 'react';
import { Trans } from '@lingui/macro';

import Cell from '../cell';
import getStats from './stats';

import './styles.css';
import { useLeaderboardFilter } from '../leaderboard-header/filter';
import Link from '../link';

export default function Leaderboard({ scores, game, mode, rankedModeKey }) {
  const stats = getStats({ game, mode, rankedModeKey });
  const {
    reset: resetFilters,
    isActive: isFilterActive,
  } = useLeaderboardFilter();

  if (scores == null) {
    return null;
  }

  if (scores?.length > 0) {
    return (
      <div>
        <div className="leaderboard">
          {scores.map((score, index) => (
            <Cell key={index} stats={stats} score={score} />
          ))}
        </div>
      </div>
    );
  } else {
    return (
      <div className="centered" style={{ margin: '50px' }}>
        {isFilterActive() ? (
          <>
            <a onClick={() => resetFilters()}>
              <Trans>Clear leaderboard filters</Trans>
            </a>
          </>
        ) : mode == null || mode?.isActive() ? (
          <Link to="/score/submit">
            <Trans>
              This leaderboard is empty. Be the first to submit a score!
            </Trans>
          </Link>
        ) : (
          <Trans>This leaderboard is not open for submissions yet.</Trans>
        )}
      </div>
    );
  }
}
