import objstr from 'obj-str';
import React from 'react';

import './styles.css';

export function TagGroup({ centered, children }) {
  const tagGroupClass = {
    ['tag-group']: true,
    ['tag-group--centered']: centered,
  };
  return <div className={objstr(tagGroupClass)}>{children}</div>;
}

export default function Tag({ color, children, ...props }) {
  const tagClass = {
    tag: true,
  };
  if (color) {
    tagClass[`tag-${color}`] = true;
  }

  return (
    <span className={objstr(tagClass)} {...props}>
      {children}
    </span>
  );
}
