import React, { forwardRef, useContext, useEffect, useState } from 'react';
import { t, Trans } from '@lingui/macro';
import { I18n, useLingui } from '@lingui/react';
import { useUserLocations } from '../../contexts/locations';
import Dialog from '../dialog';
import useLocalStorage from '../../hooks/use-storage';
import {
  Menu,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuPopover,
} from '@reach/menu-button';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import { RankingAlgorithm } from '../../tgmrank-api';

const leaderboardFilterKey = 'filter';
const LeaderboardFilterContext = React.createContext();

export function LeaderboardFilterProvider({ children }) {
  const [item, setItem, reset] = useLocalStorage(leaderboardFilterKey, {
    locations: [],
    asOf: null,
  });

  function isActive() {
    return (
      item.locations.filter(loc => loc?.length > 0).length > 0 ||
      item.asOf != null ||
      item.experimentalRankings
    );
  }

  return (
    <LeaderboardFilterContext.Provider
      value={{ filter: item, setFilter: setItem, isActive, reset }}
    >
      {children}
    </LeaderboardFilterContext.Provider>
  );
}

export function useLeaderboardFilter() {
  return useContext(LeaderboardFilterContext);
}

function LocationFilterSelector() {
  const { i18n } = useLingui();
  const { locations: locationData } = useUserLocations();
  const { filter, setFilter } = useLeaderboardFilter();

  function onSelect(event, i) {
    event.preventDefault();
    event.stopPropagation();
    event.persist();

    if (event?.target?.value == null) {
      return;
    }

    const before = filter.locations.slice(0, i);
    const after = filter.locations.slice(i + 1);

    setFilter(filter => ({
      ...filter,
      locations: [...before, event.target.value, ...after],
    }));
  }

  return (
    <>
      <ul className="list-bare">
        {filter.locations?.map((selectedLocation, i) => (
          <li key={i}>
            <select
              style={{
                margin: '0.3em',
                width: 'auto',
                display: 'inline-block',
              }}
              className="select"
              name="location"
              value={selectedLocation}
              onChange={e => onSelect(e, i)}
            >
              <option value="">{i18n._(t`Select Location`)}</option>
              {locationData?.map(location => (
                <option key={location.alpha2} value={location.alpha2}>
                  {location.name}
                </option>
              ))}
            </select>
            <button
              style={{ margin: '0.3em', display: 'inline-block' }}
              className="button"
              onClick={() =>
                setFilter(filter => ({
                  ...filter,
                  locations: filter.locations.filter((k, index) => index !== i),
                }))
              }
            >
              <Trans>Remove</Trans>
            </button>
          </li>
        ))}
      </ul>

      <button
        className="button"
        style={{ margin: '0.3em', display: 'inline-block' }}
        onClick={() =>
          setFilter(filter => ({
            ...filter,
            locations: [...filter.locations, ''],
          }))
        }
      >
        <Trans>Add</Trans>
      </button>

      <button
        className="button"
        style={{ margin: '0.3em', display: 'inline-block' }}
        onClick={() => setFilter(f => ({ ...f, locations: [] }))}
      >
        <Trans>Clear</Trans>
      </button>
    </>
  );
}

export function LeaderboardFilter() {
  const [isLocationFilterShown, showLocationFilter] = useState(false);
  const {
    filter,
    setFilter,
    reset,
    isActive: isFilterActive,
  } = useLeaderboardFilter();
  const [isDateFilterShown, showDateFilter] = useState(filter.asOf != null);
  const [startDate, setStartDate] = useState(
    filter.asOf ? new Date(filter.asOf) : new Date(),
  );

  function toggleExperimentalRankings(r) {
    setFilter(f => ({
      ...f,
      experimentalRankings: f.experimentalRankings === r ? null : r,
    }));
  }

  useEffect(() => {
    if (isDateFilterShown && startDate?.getTime() > 0) {
      setFilter(f => ({ ...f, asOf: startDate?.toISOString() }));
    }
  }, [startDate]);

  // eslint-disable-next-line no-unused-vars
  const DatePickerCustomInput = ({ value, onClick }, ref) =>
    isDateFilterShown && (
      <button className={'button'} onClick={onClick}>
        <Trans>As of {value}</Trans>
      </button>
    );

  const CustomInput = forwardRef(DatePickerCustomInput);

  return (
    <>
      <Menu>
        <MenuButton
          style={{
            color: isFilterActive() ? 'var(--highlightColor)' : null,
            fontWeight: isFilterActive() ? '600' : null,
          }}
        >
          <Trans>Filter</Trans>
        </MenuButton>
        <MenuPopover>
          <MenuItems>
            <MenuItem onSelect={() => showLocationFilter(true)}>
              <Trans>Location</Trans>
            </MenuItem>
            <MenuItem onSelect={() => showDateFilter(true)}>
              <Trans>Date</Trans>
            </MenuItem>
            <MenuItem
              onSelect={() =>
                toggleExperimentalRankings(RankingAlgorithm.Percentile)
              }
            >
              <Trans>Experimental Rankings</Trans>
            </MenuItem>
            <MenuItem
              onSelect={() =>
                toggleExperimentalRankings(RankingAlgorithm.SimpleMax)
              }
            >
              <Trans>Experimental Rankings</Trans> 2
            </MenuItem>
            {isFilterActive() && (
              <MenuItem
                onSelect={() => {
                  showDateFilter(false);
                  setStartDate(new Date());
                  reset();
                }}
              >
                <Trans>Clear</Trans>
              </MenuItem>
            )}
          </MenuItems>
        </MenuPopover>
      </Menu>
      <Dialog
        aria-label="Location Filter Dialog"
        isOpen={isLocationFilterShown}
        close={() => showLocationFilter(false)}
      >
        <LocationFilterSelector />
      </Dialog>
      <DatePicker
        disabled={!isDateFilterShown}
        selected={startDate}
        maxDate={new Date()}
        onChange={date => setStartDate(date)}
        // autoFocus={true}
        customInput={<CustomInput />}
        // startOpen
        // withPortal
        shouldCloseOnSelect
        // disabled
        // inline
      />
    </>
  );
}
