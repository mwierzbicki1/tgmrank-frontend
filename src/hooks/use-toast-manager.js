import React from 'react';
import { useToasts } from 'react-toast-notifications';

export function useToastManager() {
  const toastManager = useToasts();

  toastManager.addSuccess = message => {
    toastManager.addToast(message, {
      appearance: 'success',
      autoDismiss: true,
    });
  };

  toastManager.addError = (message, error) => {
    if (typeof message != 'string') {
      message = message.error;
    }

    if (error != null) {
      message = (
        <span>
          {message}
          <br />
          <br />
          {error.response != null && (
            <>
              {error.response.status} {error.response.statusText}
            </>
          )}
          <br />
          {error.error}
        </span>
      );
    }

    toastManager.addToast(message, {
      appearance: 'error',
      autoDismiss: true,
    });
  };

  return toastManager;
}
