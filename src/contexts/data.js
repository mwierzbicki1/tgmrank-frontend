import React, { useContext } from 'react';

const DataContext = React.createContext();

export function DataProvider({ data, children }) {
  const value = {
    ...(typeof window !== 'undefined' ? window.data : null),
    data,
  };

  return <DataContext.Provider value={value}>{children}</DataContext.Provider>;
}

export function useData() {
  return useContext(DataContext);
}
