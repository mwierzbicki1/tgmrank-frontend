import React, { useContext, useState } from 'react';
import Cookie from 'js-cookie';

const LanguageContext = React.createContext();

export function LanguageProvider({ children }) {
  const [currentLanguage, setLanguage] = useState(() => Cookie.get('locale'));

  function changeLanguage(lang) {
    Cookie.set('locale', lang, {
      sameSite: 'strict',
      secure: !process.env.TGMRANK_BASE_URL.includes('localhost'),
    });
    setLanguage(lang);
    window.location.reload();
  }

  return (
    <LanguageContext.Provider value={{ currentLanguage, changeLanguage }}>
      {children}
    </LanguageContext.Provider>
  );
}

export function useLanguage() {
  return useContext(LanguageContext);
}
