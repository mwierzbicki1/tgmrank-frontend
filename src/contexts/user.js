import React, { useContext, useEffect, useState } from 'react';
import TgmRank, { LegacyUserRole } from '../tgmrank-api';
import { localStorageWrapper } from '../server-helpers';

const UserContext = React.createContext();

const userKey = 'tgmrank_user';

export function isNotBanned(user) {
  return user?.role.toLowerCase() !== LegacyUserRole.Banned;
}

export function isAdmin(user) {
  return user?.role?.toLowerCase() === LegacyUserRole.Admin;
}

export function canVerifyScores(user) {
  const role = user?.role.toLowerCase();
  return role === LegacyUserRole.Verifier || role === LegacyUserRole.Admin;
}

export function UserProvider({ children }) {
  const storage = localStorageWrapper();

  function getUserData() {
    const rawData = storage?.getItem(userKey);
    if (rawData) {
      return JSON.parse(rawData);
    }
    return null;
  }

  const [user, setUser] = useState(getUserData());
  const isLoggedIn = !!user;

  async function verifyLogin() {
    const userDetails = await TgmRank.loginVerify();

    if (userDetails?.success != null && userDetails.success === false) {
      logout();
    } else {
      login(userDetails);
    }

    return userDetails;
  }

  useEffect(() => {
    verifyLogin();
  }, []);

  const login = user => {
    storage?.setItem(userKey, JSON.stringify(user));
    setUser(user);
  };

  const logout = () => {
    storage?.removeItem(userKey);
    setUser(null);
  };

  return (
    <UserContext.Provider
      value={{
        user,
        login,
        logout,
        verifyLogin,
        isLoggedIn,
      }}
    >
      {children}
    </UserContext.Provider>
  );
}

export function useUser() {
  return useContext(UserContext);
}
