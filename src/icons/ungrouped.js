import React from 'react';

export default function IconGrouped(props) {
  return (
    <svg {...props} viewBox="0 0 24 24">
      <g transform="translate(2 5)">
        <rect width="20" height="2" rx="1" />
        <rect y="4" width="20" height="2" rx="1" />
        <rect y="8" width="20" height="2" rx="1" />
        <rect y="12" width="20" height="2" rx="1" />
      </g>
    </svg>
  );
}
