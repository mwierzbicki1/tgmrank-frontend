msgid ""
msgstr ""
"POT-Creation-Date: 2019-02-08 10:45-0500\n"
"Mime-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: @lingui/cli\n"
"Language: fr\n"
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Plural-Forms: \n"

#: src/pages/add-score/index.js:1014
msgid "..."
msgstr ""

#: src/pages/score/index.js:205
msgid "<0/> by <1/> on {0}"
msgstr ""

#: src/pages/death-calculator.js:88
msgid "<0>Death Series of 5</0> Calculator"
msgstr "Algorithme de calcul pour les <0>5 Parties de Death Consécutives</0>"

#: src/pages/home/index.js:110
msgid "<0>Notable scores</0> are scores that land in the top 10 of any of the <1/> leaderboards."
msgstr ""

#: src/components/rival/index.js:108
msgid "<0>Reverse Rivals:</0>"
msgstr ""

#: src/components/passed-players.js:46
msgid "<0>{0} more</0> players"
msgstr "<0>{0} autres</0> joueurs"

#: src/pages/score/index.js:159
msgid "<0>{0}</0> Points"
msgstr "<0>{0}</0> points"

#: src/components/footer/index.js:36
#: src/components/header/index.js:154
msgid "About/Contact"
msgstr "A propos/Contact"

#: src/components/achievement/index.js:97
msgid "Achieved On"
msgstr "Réalisé le"

#: src/components/achievement/index.js:92
msgid "Achievement"
msgstr "Succès"

#: src/pages/player/index.js:165
msgid "Achievements"
msgstr "Succès"

#: src/pages/challenges/index.js:242
msgid "Active"
msgstr ""

#: src/components/leaderboard-header/filter.js:121
#: src/pages/add-score/index.js:184
#: src/pages/death-calculator.js:166
msgid "Add"
msgstr "Ajouter"

#: src/components/rival/index.js:80
msgid "Add Rival"
msgstr "Ajouter un rival"

#: src/pages/add-score/index.js:894
msgid "Add score"
msgstr "Soumettre un record"

#: src/components/achievement/index.js:69
#: src/components/leaderboard-header/index.js:251
msgid "All"
msgstr ""

#: src/components/disclaimer/index.js:10
msgid "Already see your name or scores here? Contact an admin via <0>Discord</0> or <1>email</1>"
msgstr "Vos scores et/ou noms sont déjà présents sur ce site ? Contactez un administrateur via <0>Discord</0> ou <1>Email</1>"

#: src/components/header/index.js:59
#: src/pages/challenges/index.js:260
msgid "Archive"
msgstr ""

#: src/pages/add-score/index.js:1033
msgid "Are you sure you want to reset this form?"
msgstr "Êtes-vous sûr de vouloir réinitialiser ce formulaire?"

#: src/components/leaderboard-header/filter.js:165
msgid "As of {value}"
msgstr "Tel qu'il était le {value}"

#: src/pages/settings/index.js:288
msgid "Avatar"
msgstr "Photo de profil"

#: src/pages/home/index.js:48
msgid "Browse Leaderboards"
msgstr "Voir les classements"

#: src/pages/add-score/index.js:1047
#: src/pages/add-score/index.js:1133
msgid "Cancel"
msgstr "Annuler"

#: src/pages/settings/index.js:244
msgid "Cannot Update Username"
msgstr ""

#: src/pages/settings/index.js:54
#: src/pages/settings/index.js:100
msgid "Change Password"
msgstr "Modifier le mot de passe"

#: src/pages/settings/index.js:203
msgid "Change Username"
msgstr ""

#: src/pages/settings/index.js:98
msgid "Changing Password..."
msgstr "Modification du mot de passe..."

#: src/pages/add-score/index.js:905
msgid "Choose which game and mode the score was achieved on."
msgstr "Sélectionnez le jeu et le mode sur lesquels ce score a été obtenu."

#: src/components/leaderboard-header/filter.js:129
#: src/components/leaderboard-header/filter.js:212
msgid "Clear"
msgstr "Effacer"

#: src/components/score/index.js:55
msgid "Clear %"
msgstr "Pourcentage de complétion"

#: src/components/leaderboard/index.js:38
msgid "Clear leaderboard filters"
msgstr "Effacer les filtres du classement"

#: src/pages/add-score/index.js:110
msgid "Comment"
msgstr "Commentaire"

#: src/components/forgot-password-form/index.js:151
msgid "Confirm Password"
msgstr "Confirmer le mot de passe"

#: src/components/leaderboard/stats.js:113
msgid "Contributing Scores"
msgstr ""

#: src/pages/settings/index.js:294
msgid "Current Avatar"
msgstr "Photo de profil actuelle"

#: src/pages/add-score/index.js:289
msgid "Current details"
msgstr "Infos actuelles"

#: src/pages/challenges/index.js:119
msgid "DEATH TOLL"
msgstr ""

#: src/components/leaderboard-header/filter.js:188
msgid "Date"
msgstr "Date"

#: src/pages/score/index.js:240
msgid "Edit score"
msgstr "Modifier ce score"

#: src/components/sign-up-form.js:54
msgid "Email"
msgstr "Email"

#: src/components/leaderboard-header/index.js:339
#: src/pages/challenges/index.js:237
msgid "Events"
msgstr ""

#: src/components/leaderboard-header/filter.js:195
#: src/components/leaderboard-header/filter.js:202
msgid "Experimental Rankings"
msgstr ""

#: src/components/leaderboard-header/filter.js:180
msgid "Filter"
msgstr "Filtre"

#: src/pages/login/index.js:88
msgid "Forgot Password"
msgstr "Oubli de mot de passe"

#: src/components/login-form/index.js:107
msgid "Forgot Password?"
msgstr "Mot de passe oublié ?"

#: src/components/activity-delta.js:79
#: src/pages/add-score/index.js:902
#: src/pages/add-score/index.js:913
msgid "Game"
msgstr "Jeu"

#: src/components/leaderboard-header/index.js:207
msgid "Game Info"
msgstr "A propos du jeu"

#: src/components/score/index.js:45
msgid "Grade"
msgstr "Grade"

#: src/components/score-improvements.js:28
msgid "Grade increased from <0><1>{0}</1></0> to <2><3>{1}</3></2>"
msgstr "Grade amélioré de <0><1>{0}</1></0> à <2><3>{1}</3></2>"

#: src/components/score/index.js:50
msgid "Hanabi"
msgstr "Hanabi"

#: src/components/scores.js:54
msgid "Hide score history"
msgstr "Masquer l'historique des records"

#: src/components/sign-up-form.js:114
msgid "I agree to the <0>Terms of Service and Privacy Policy</0>"
msgstr "J'accepte <0>les Conditions d'utilisation</0>"

#: src/pages/add-score/index.js:152
msgid "Image"
msgstr "Image"

#: src/pages/home/index.js:73
msgid "Latest Scores"
msgstr "Derniers Records"

#: src/components/leaderboard-header/index.js:217
msgid "Legacy Leaderboard"
msgstr "Ancien classement"

#: src/components/score/index.js:55
msgid "Level"
msgstr "Niveau"

#: src/components/score-improvements.js:43
msgid "Level improved by <0>{0} levels</0>"
msgstr "Niveau amélioré de <0>{0} niveaux</0>"

#: src/components/leaderboard-header/filter.js:185
#: src/pages/settings/index.js:141
msgid "Location"
msgstr "Localisation"

#: src/components/login-form/index.js:116
msgid "Logging in..."
msgstr "Connexion au compte..."

#: src/components/header/index.js:177
#: src/components/login-form/index.js:116
#: src/pages/login/index.js:41
msgid "Login"
msgstr "Connexion"

#: src/components/header/index.js:157
msgid "Logout"
msgstr "Déconnexion"

#: src/components/leaderboard/stats.js:138
msgid "Missing Scores"
msgstr ""

#: src/components/activity-delta.js:87
#: src/pages/add-score/index.js:934
msgid "Mode"
msgstr "Mode"

#: src/components/cell/index.js:52
msgid "New"
msgstr "New"

#: src/pages/settings/index.js:305
msgid "New Avatar (100kb file size limit)"
msgstr "Nouvelle photo de profil (100kb max)"

#: src/pages/settings/index.js:72
msgid "New Password"
msgstr "Nouveau mot de passe"

#: src/pages/settings/index.js:83
msgid "New Password (again)"
msgstr "Nouveau mot de passe (encore une fois)"

#: src/pages/settings/index.js:215
msgid "New Username"
msgstr ""

#: src/components/rival/index.js:74
msgid "No Rival Slots Available"
msgstr "La limite de rivaux a été atteinte."

#: src/pages/death-calculator.js:119
msgid "No games added yet."
msgstr "Aucune partie n'a étée ajoutée"

#: src/pages/home/index.js:74
msgid "Notable Scores"
msgstr ""

#: src/pages/settings/index.js:205
msgid "Note: You can only change your username once every 60 days. Be careful."
msgstr ""

#: src/pages/settings/index.js:61
msgid "Old Password"
msgstr "Ancien mot de passe"

#: src/pages/add-score/index.js:87
msgid "Optional"
msgstr "Facultatif"

#: src/pages/add-score/index.js:153
msgid "Other"
msgstr "Autre"

#: src/components/activity-delta.js:71
#: src/components/header/index.js:47
#: src/pages/leaderboard/index.js:106
msgid "Overall"
msgstr "Scores Globaux"

#: src/pages/player/index.js:141
msgid "Overview"
msgstr "Détails"

#: src/components/passed-players.js:102
msgid "Passed <0/> in {title} rankings."
msgstr "A dépassé <0/> dans le classement {title}"

#: src/components/forgot-password-form/index.js:135
#: src/components/login-form/index.js:77
#: src/components/sign-up-form.js:87
msgid "Password"
msgstr "Mot de passe"

#: src/pages/death-calculator.js:95
msgid "Past {gamesToShow} games"
msgstr "Les dernières {gamesToShow} parties"

#: src/pages/score/index.js:185
msgid "Platform: {0}"
msgstr ""

#: src/pages/add-score/index.js:405
msgid "Player"
msgstr "Joueur"

#: src/pages/add-score/index.js:71
msgid "Please review the <0><1>Proof Policy</1></0> to ensure a smooth score validation process."
msgstr ""

#: src/pages/add-score/index.js:994
msgid "Please review the <0>Proof Policy</0> to ensure a smooth score validation process."
msgstr ""

#: src/components/score/index.js:41
msgid "Points"
msgstr "Points"

#: src/pages/add-score/index.js:1092
msgid "Preview"
msgstr ""

#: src/components/header/index.js:138
#: src/pages/settings/index.js:337
msgid "Profile"
msgstr "Profil"

#: src/pages/add-score/index.js:326
#: src/pages/add-score/index.js:989
msgid "Proof"
msgstr "Preuve"

#: src/components/cell/index.js:59
msgid "Rank"
msgstr "Rang"

#: src/components/activity-delta.js:28
msgid "Ranked <0/>"
msgstr "Classé <0/>"

#: src/pages/score/index.js:170
msgid "Ranked <0><1/></0>"
msgstr "Classé <0><1/></0>"

#: src/components/sign-up-form.js:132
msgid "Register"
msgstr "Créer un compte"

#: src/components/sign-up-form.js:130
msgid "Registering..."
msgstr "Création du compte..."

#: src/components/login-form/index.js:102
msgid "Remember me"
msgstr "Se souvenir de moi"

#: src/components/leaderboard-header/filter.js:105
#: src/pages/add-score/index.js:175
msgid "Remove"
msgstr "Effacer"

#: src/components/rival/index.js:68
msgid "Remove Rival"
msgstr "Enlever ce rival"

#: src/pages/death-calculator.js:177
msgid "Reset"
msgstr "Réinitialiser"

#: src/pages/add-score/index.js:1006
#: src/pages/add-score/index.js:1029
#: src/pages/add-score/index.js:1044
msgid "Reset Form"
msgstr "Réinitialiser le formulaire"

#: src/components/forgot-password-form/index.js:170
#: src/pages/login/index.js:104
msgid "Reset Password"
msgstr "Réinitialiser le mot de passe"

#: src/components/forgot-password-form/index.js:168
msgid "Resetting Password..."
msgstr "Réinitialisation du mot de passe..."

#: src/pages/player/index.js:173
msgid "Rivals"
msgstr "Rivaux"

#: src/components/score/index.js:50
#: src/pages/add-score/index.js:286
msgid "Score"
msgstr "Score"

#: src/pages/add-score/index.js:309
#: src/pages/add-score/index.js:971
msgid "Score Details"
msgstr "Détails du record"

#: src/components/score-improvements.js:17
msgid "Score improved by <0>{0} points</0>"
msgstr "Score amélioré de <0>{0} points</0>"

#: src/pages/settings/index.js:338
msgid "Security"
msgstr "Sécurité"

#: src/components/leaderboard-header/filter.js:88
#: src/pages/settings/index.js:147
msgid "Select Location"
msgstr "Choisir une localisation"

#: src/pages/add-score/index.js:922
msgid "Select game"
msgstr "Sélectionnez le jeu"

#: src/pages/add-score/index.js:952
msgid "Select mode"
msgstr "Sélectionnez le mode"

#: src/pages/add-score/index.js:424
msgid "Select player"
msgstr "Choisissez le joueur"

#: src/pages/add-score/index.js:150
msgid "Select type"
msgstr "Choisissez un type"

#: src/components/forgot-password-form/index.js:67
msgid "Send Forgot Password Email"
msgstr "Envoyer l'email d'oubli du mot de passe"

#: src/components/forgot-password-form/index.js:65
msgid "Sending Forgot Password Email..."
msgstr "Envoi de l'email d'oubli du mot passe..."

#: src/components/header/index.js:141
msgid "Settings"
msgstr "Paramètres"

#: src/components/scores.js:59
msgid "Show score history"
msgstr "Afficher l'historique des records"

#: src/pages/add-score/index.js:329
msgid "Show us your (new) moves."
msgstr "T'en as des nouvelles ?"

#: src/pages/add-score/index.js:992
msgid "Show us your moves."
msgstr "(stp)"

#: src/components/header/index.js:174
#: src/pages/login/index.js:42
msgid "Sign up"
msgstr "S'enregistrer"

#: src/components/score/index.js:45
msgid "Stage"
msgstr "Stage"

#: src/pages/add-score/index.js:343
#: src/pages/add-score/index.js:1014
#: src/pages/add-score/index.js:1129
msgid "Submit"
msgstr "Soumettre"

#: src/components/header/index.js:103
msgid "Submit Score"
msgstr "Soumettre un record"

#: src/pages/score/index.js:191
msgid "Submitted on <0>{0}</0>"
msgstr ""

#: src/pages/add-score/index.js:974
msgid "Tell us more about your achievement."
msgstr "Dis-nous en plus à propos de cette run"

#: src/components/leaderboard/label.js:53
msgid "The <0>{0} <1/> Leaderboard</0> is based on your performance in the following modes:"
msgstr "Le <0>classement {0} <1/></0> est calculé selon tes performances dans les modes suivants :"

#: src/components/leaderboard/index.js:43
msgid "This leaderboard is empty. Be the first to submit a score!"
msgstr "Ce classement est vide... Soyez le premier à publier un record !"

#: src/components/leaderboard/index.js:48
msgid "This leaderboard is not open for submissions yet."
msgstr ""

#: src/components/leaderboard-header/index.js:85
msgid "This mode does not have formal rules yet. Be cautious and use your best judgment :)"
msgstr "Ce mode ne possède pas de règles écrites, donc soyez prudent et agissez selon votre morale :)"

#: src/pages/score/index.js:229
msgid "This score has not been reviewed yet."
msgstr ""

#: src/pages/add-score/index.js:1112
msgid "This score may need video proof."
msgstr ""

#: src/components/score/index.js:58
msgid "Time"
msgstr "Temps"

#: src/components/time-delta.js:27
msgid "Time improved by <0>{delta} s</0>"
msgstr "Temps amélioré de <0>{delta} s</0>"

#: src/pages/404.js:31
msgid "Try your luck on the <0>Homepage</0>"
msgstr "Essayez d'aller sur la <0>page principale</0>..."

#: src/components/leaderboard-header/index.js:212
msgid "Twitch"
msgstr ""

#: src/components/activity-delta.js:56
msgid "Unranked"
msgstr "Non-classé"

#: src/pages/challenges/index.js:251
msgid "Upcoming"
msgstr ""

#: src/pages/settings/index.js:319
msgid "Update Avatar"
msgstr "Mettre à jour la photo de profil"

#: src/pages/settings/index.js:157
msgid "Update Location"
msgstr "Mettre à jour la localisation"

#: src/pages/settings/index.js:246
msgid "Update Username"
msgstr ""

#: src/pages/add-score/index.js:279
msgid "Update score"
msgstr "Mettre à jour le score"

#: src/pages/add-score/index.js:312
msgid "Update some stuff"
msgstr "C'est ici qu'il faut mettre à jour des infos sur la run"

#: src/pages/settings/index.js:317
msgid "Updating Avatar..."
msgstr "Mise à jour de la photo de profil..."

#: src/pages/settings/index.js:361
msgid "User Settings"
msgstr "Paramètres de l'utilisateur"

#: src/components/login-form/index.js:61
#: src/components/sign-up-form.js:71
msgid "Username"
msgstr "Pseudo"

#: src/pages/settings/index.js:226
msgid "Username history"
msgstr ""

#: src/components/forgot-password-form/index.js:49
msgid "Username or Email"
msgstr "Pseudo ou email"

#: src/pages/add-score/index.js:151
msgid "Video"
msgstr "Vidéo"

#: src/components/leaderboard-header/index.js:167
#: src/pages/add-score/index.js:939
msgid "View Rules"
msgstr "Accéder aux règles"

#: src/pages/login/index.js:56
msgid "Welcome to TGM Rank"
msgstr "Bienvenue sur les classements TGM"

#: src/pages/add-score/index.js:408
msgid "Who did this?"
msgstr "Qui a réalisé ça ?"

#: src/pages/death-calculator.js:134
msgid "You got a new best score!"
msgstr "Vous avez réalisé un nouveau record !"

#: src/pages/death-calculator.js:123
msgid "Your best series score is: <0>{bestSeries}</0>"
msgstr "Votre meilleur score de série est : <0>{bestSeries}</0>"

#: src/pages/death-calculator.js:138
msgid "Your current score is:"
msgstr "Votre score actuel est :"

#: src/pages/death-calculator.js:145
msgid "Your personal best is: <0>{seriesPersonalBest}</0>"
msgstr "Votre meilleur score personnel est : <0>{seriesPersonalBest}</0>"

#: src/pages/settings/index.js:148
msgid "[Unset]"
msgstr "[Non-défini]"

#: src/components/passed-players.js:25
#: src/components/passed-players.js:39
msgid "comma"
msgstr ", "

#: src/components/passed-players.js:28
#: src/components/passed-players.js:45
msgid "comma.and"
msgstr " et "

#: src/components/recent-activity.js:141
msgid "infinite-scroll.enable"
msgstr "Défilement Infini"

#: src/components/recent-activity.js:147
msgid "infinite-scroll.end"
msgstr "Il n'y a plus de résultats"

#: src/components/activity-delta.js:40
msgid "{0, plural, one {# point} other {# points}}"
msgstr "{0, plural, one {# point} other {# points}}"

#: src/components/leaderboard/label.js:13
msgid "{leaderboardName, select, main {Main} extended {Extended} secret {Secret} big {Big} world {World} worldExtended {World (Extended)}}"
msgstr "{leaderboardName, select, main {Principal} extended {Étendu} secret {Secret} big {Big} world {World} worldExtended {World (Étendu)}}"

#: src/components/leaderboard/rank.js:10
msgid "{rank, selectordinal, one {#st} two {#nd} few {#rd} other {#th}}"
msgstr "{rank, selectordinal, one {#er} two {#e} few {#e} other {#e}}"

#: src/components/score/index.js:28
msgid "{status, select, unverified {Unverified} pending {Pending} legacy {Legacy} rejected {Rejected} verified {Verified} accepted {Accepted}}"
msgstr ""

#: src/components/time-ago.js:30
msgid "{time, plural, one {1 day ago} other {# days ago}}"
msgstr "{time, plural, one {il y a 1 jour} other {il y a # jours}}"

#: src/components/time-ago.js:28
msgid "{time, plural, one {1 month ago} other {# months ago}}"
msgstr "{time, plural, one {il y a 1 mois} other {il y a # mois}}"

#: src/components/time-ago.js:29
msgid "{time, plural, one {1 week ago} other {# weeks ago}}"
msgstr "{time, plural, one {il y a 1 semaine} other {il y a # semaines}}"

#: src/components/time-ago.js:27
msgid "{time, plural, one {1 year ago} other {# years ago}}"
msgstr "{time, plural, one {il y a 1 an} other {il y a # ans}}"

#: src/components/time-ago.js:32
msgid "{time, plural, one {a minute ago} other {# minutes ago}}"
msgstr "{time, plural, one {il y a une minute} other {il y a # minutes}}"

#: src/components/time-ago.js:31
msgid "{time, plural, one {an hour ago} other {# hours ago}}"
msgstr "{time, plural, one {il y a une heure} other {il y a # heures}}"
