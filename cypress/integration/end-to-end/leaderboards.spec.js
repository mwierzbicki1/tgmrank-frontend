function assertLeaderboardHasAnEntry() {
  // Rank
  cy.get('.leaderboard > :nth-child(1) > :nth-child(1)')
    .invoke('text')
    .then(parseInt)
    .should('be.a', 'number');

  // Points
  // TODO: Add data attributes to stats display
  cy.get(':nth-child(1) > .number > :nth-child(1)')
    .invoke('text')
    .then(parseInt)
    .should('be.a', 'number');
}

describe('Leaderboards', () => {
  const mainPagePaths = [
    '/overall',
    '/overall/extended',
    '/tgm1',
    '/tgm1/extended',
    '/tap',
    '/tap/extended',
    '/ti',
    '/ti/extended',
  ];

  for (const path of mainPagePaths) {
    it(`Should show aggregate leaderboards: ${path}`, () => {
      cy.visit(path);
      assertLeaderboardHasAnEntry();
    });
  }

  it('Should allow game aliases', () => {
    cy.visit('/tgm2');
    assertLeaderboardHasAnEntry();

    cy.visit('/tgm3');
    assertLeaderboardHasAnEntry();
  });

  it('Should allow mode aliases', () => {
    cy.visit('/tgm2/tgm-plus');
    assertLeaderboardHasAnEntry();

    cy.visit('/tgm3/shirase');
    assertLeaderboardHasAnEntry();
  });

  it('Should show mode leaderboards', () => {
    cy.visit('/tgm1/masteR');
    assertLeaderboardHasAnEntry();

    cy.visit('/tap/death');
    assertLeaderboardHasAnEntry();

    cy.visit('/ti/shirase (C)');
    assertLeaderboardHasAnEntry();
  });

  it('Should let users log in', () => {
    const username = Cypress.env('USER_NAME');
    const password = Cypress.env('USER_PASSWORD');

    if (username && password) {
      cy.visit('/login');

      cy.login(username, password);

      // Redirected back to home page
      cy.validatePath('/');

      cy.get('[data-action="submit-score"]').click();
      cy.validatePath('/score/submit');

      cy.get('.user-menu').should('be.visible');
      cy.get('.user-menu').click();

      cy.get('[href="/settings"]').should('be.visible');
      cy.get('[href="/settings"]').click();
      cy.validatePath('/settings');

      cy.get('.user-menu').should('be.visible');
      cy.get('.user-menu').click();

      cy.get('[href="/logout"]').should('be.visible');
      cy.get('[href="/logout"]').click();

      cy.validatePath('/');
    }
  });

  it('Should show death calculator', () => {
    cy.visit('/tools/death');
    cy.get('#add').should('exist');
    cy.url().should('not.include', '/not-found');
  });

  it('Should show about page', () => {
    cy.visit('/about');
    cy.url().should('not.include', '/not-found');
    cy.get('.about').should('exist');
  });
});
